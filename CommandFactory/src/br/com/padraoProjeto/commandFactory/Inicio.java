package br.com.padraoProjeto.commandFactory;

public class Inicio {

	public static void main(String[] args) {

		AplicativoComFactory executarAcao = new AplicativoComFactory();

		executarAcao.buttonEnviar_clicked();

		executarAcao.buttonGerarPDF_clicked();

		executarAcao.buttonNovoPedido_clicked();

		executarAcao.buttonExcluirPedido_clicked();

		executarAcao.buttonGerarPorEmail_clicked();
	}

}
