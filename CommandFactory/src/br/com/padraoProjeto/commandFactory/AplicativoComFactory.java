package br.com.padraoProjeto.commandFactory;

public class AplicativoComFactory {

	private CommandFactory commandFactory;

	public AplicativoComFactory() {
		commandFactory = new CommandFactory();
	}
	
	public void buttonNovoPedido_clicked() {
		BaseCommand comandoGerarNovoPedido = commandFactory.create("NovoPedido");
		comandoGerarNovoPedido.executar();
	}

	public void buttonEnviar_clicked() {
		BaseCommand comandoEnviar = commandFactory.create("GerarPorEmail");
		comandoEnviar.executar();
	}

	public void buttonGerarPDF_clicked() {
		BaseCommand comandoGerarPDF = commandFactory.create("GerarPdf");
		comandoGerarPDF.executar();
	}

	public void buttonGerarPorEmail_clicked() {
		BaseCommand comandoGerarPorPDF = commandFactory.create("GerarPorEmail");
		comandoGerarPorPDF.executar();
	}	
	
	public void buttonExcluirPedido_clicked() {
		BaseCommand comandoExcluirPedido = commandFactory.create("ExcluirPedido");
		comandoExcluirPedido.executar();
	}


	
	
}
