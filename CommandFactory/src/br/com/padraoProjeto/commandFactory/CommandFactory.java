package br.com.padraoProjeto.commandFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CommandFactory {

Properties apelidosComandos = new Properties();
	
	public CommandFactory () {
		try {
			InputStream f = getClass().getResourceAsStream("commands.properties");
			apelidosComandos.load(f);
			f.close();
		} catch (IOException e) {
  System.out.println(" O arquivo ou classe procurado n�o existe!");
}
		}

	public BaseCommand create(String name){
		BaseCommand baseCommand = null;
		String stringClasse = apelidosComandos.getProperty(name);
		Class classe;
		try {
			classe = Class.forName(stringClasse);
			Object object = classe.newInstance();
			baseCommand = (BaseCommand) object;
		} catch (Exception e) {
  System.out.println(" Opss..!! Erro na  cria��o do objeto!"); }
  
  return baseCommand; }
}
