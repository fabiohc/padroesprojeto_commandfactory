package br.com.padraoProjeto.commandFactory;

public class GerarPorEmail implements BaseCommand {

	public GerarPorEmail() {
	}

	public void executar() {
		System.out.println("PDF enviado por E-mail com sucesso!");
	}
}